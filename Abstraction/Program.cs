﻿

using Abstraction.Utilities;
using Abstraction.Utilities.Employees;
#region First Example of abstraction
List<DataAccess> _dbs = new List<DataAccess>()
{
    new MSSQLDataAccess(),
    new PostgreSQLDataAccess(),
};

foreach (var db in _dbs)
{
    Console.WriteLine(db.LoadConnecitonString("MSSQL"));
    db.LoadData("");
    db.SaveData("");

    Console.WriteLine();
}
#endregion

Console.WriteLine("............................................");

#region Second example of abstraction
FullTimeEmployee fullTimeEmployee = new FullTimeEmployee()
{
    Id = 1,
    FirstName = "Rajib",
    LastName = "Sarker",
    YearlySalary = 100000
};

Console.WriteLine(fullTimeEmployee.GetFullName() +" working as full time employee.");
Console.WriteLine(fullTimeEmployee.GetMonthlySalary());

Console.WriteLine(".....................");

PartTimeEmployee partTimeEmployee = new PartTimeEmployee()
{
    Id = 2,
    FirstName = "Raju",
    LastName = "Sarker",
    HourlySalary = 500,
    TotalHours = 40
};
Console.WriteLine(partTimeEmployee.GetFullName() + " working as part time employee.");
Console.WriteLine(partTimeEmployee.GetMonthlySalary());


//Employee employee = new Employee()
//{
//    Id = 3,
//    FirstName = "ABC",
//    LastName = "XYZ"
//};

//Console.WriteLine(employee.GetFullName() + " from base class.");
#endregion

Console.ReadKey();
