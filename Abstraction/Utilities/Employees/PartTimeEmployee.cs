﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstraction.Utilities.Employees
{
    public class PartTimeEmployee : Employee
    {
        public int HourlySalary { get; set; }
        public int TotalHours { get; set; }


        public override double GetMonthlySalary()
        {
            return this.HourlySalary * this.TotalHours;
        }
    }
}
