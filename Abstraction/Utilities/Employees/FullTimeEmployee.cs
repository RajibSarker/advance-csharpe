﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstraction.Utilities.Employees
{
    public class FullTimeEmployee : Employee
    {
        public int YearlySalary { get; set; }


        public override double GetMonthlySalary()
        {
            return this.YearlySalary / 12;
        }
    }
}
