﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstraction.Utilities
{
    public interface IDataAccess
    {
        string LoadConnecitonString(string connection);
        void LoadData(string msg);
        void SaveData(string msg);
    }
}
