﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstraction.Utilities
{
    public abstract class DataAccess
    {
        public virtual string LoadConnecitonString(string connection)
        {
            Console.WriteLine("Loading Database ConnectionStrings.");
            return "<ConnecitonString>";
        }

        public abstract void LoadData(string msg);
        public abstract void SaveData(string msg);
    }
}
