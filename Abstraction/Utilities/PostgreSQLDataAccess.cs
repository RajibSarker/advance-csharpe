﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstraction.Utilities
{
    public class PostgreSQLDataAccess : DataAccess
    {

        public override string LoadConnecitonString(string connection)
        {
            string output = base.LoadConnecitonString(connection);
            return output + " from child class";
        }
        public override void LoadData(string msg)
        {
            Console.WriteLine("Loading Data Using PostgreSQL Server.");
        }

        public override void SaveData(string msg)
        {
            Console.WriteLine("Saving Data Using PostgreSQL Server.");
        }
    }
}
