﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstraction.Utilities
{
    public class MSSQLDataAccess : DataAccess
    {

        public override void LoadData(string msg)
        {
            Console.WriteLine("Loading Data Using MSSQL Server.");
        }

        public override void SaveData(string msg)
        {
            Console.WriteLine("Saving Data Using MSSQL Server.");
        }
    }
}
