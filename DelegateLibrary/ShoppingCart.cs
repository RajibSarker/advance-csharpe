﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateLibrary
{
    public class ShoppingCart
    {
        // deligates
        public delegate void MentionDiscount(decimal subTotal);
        
        
        public ShoppingCart()
        {
            Products = new List<Product>();
        }
        public List<Product> Products { get; set; }
        public decimal GenerateSubTotal(MentionDiscount discount, Func<decimal,
            decimal> calculateDiscount, Action<string> alert)
        {
            if (Products == null || !Products.Any()) return 0;

            decimal subTotal = Products.Sum(c => c.Price);
            alert("Hello I'm as action delegate.");
            discount(subTotal);
            return calculateDiscount(subTotal);
        }
    }
}
