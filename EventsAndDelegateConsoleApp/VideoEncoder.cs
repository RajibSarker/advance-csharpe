﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsAndDelegateConsoleApp
{
    public class VideoEncoder
    {
        // define the delegate
        public delegate void VideoEncodedEventHandler(object source, EventArgs args);

        // define the method based on the delegate
        public event VideoEncodedEventHandler VideoEncoded;
        public void Encode(Video video)
        {
            Console.WriteLine("Encoding the video...");
            Thread.Sleep(3000);
            OnVideoEncoded();
        }

        // raise the event
        protected virtual void OnVideoEncoded()
        {
            if(VideoEncoded!=null)
                VideoEncoded(this, EventArgs.Empty);
        }
    }
}
