﻿// video encoder...

using EventsAndDelegateConsoleApp;

var video = new Video() { Title = "Video-1" };
var videoEncoder = new VideoEncoder();
var mailService = new MailService();

videoEncoder.VideoEncoded += mailService.OnVideoEncoded;
videoEncoder.Encode(video);






public class MailService
{
    public void OnVideoEncoded(object source, EventArgs args)
    {
        Console.WriteLine("Mail Service: Mail is sending...");
    }
}
