﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoLibrary
{
    public class AccessModifierCalling2:AccessModifier
    {
        protected void ProtectedMyMethod()
        {
            base.PublicMethod();
            base.ProtectedMethod();
            base.InternalMethod();
            base.PrivateProtectedMethod();
        }
    }
}
