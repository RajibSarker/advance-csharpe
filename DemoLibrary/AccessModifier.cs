﻿namespace DemoLibrary
{
    public class AccessModifier
    {
        //public method accessibilitiy is everywhere what we want
        //    (if need then add reference to another class)
        public void PublicMethod()
        {

        }
        // private methods accessibility is in declared class
        private void Demo()
        {

        }
        private void Demo2()
        {
            Demo();
        }


        // protected methods can use by inherit in anywhere if add reference
        protected void ProtectedMethod()
        {


        }

        // internal methods can access on only the project
        internal void InternalMethod()
        {

        }

        // private protected
        private protected void PrivateProtectedMethod()
        {

        }



        
    }
}