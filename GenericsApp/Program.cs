﻿

using GenericsApp.Models;
using GenericsApp.WithGenerics;
using GenericsApp.WithoutGenerics;


DemonstrateTextFileStorage();

void DemonstrateTextFileStorage()
{
    List<People> peoples = new List<People>();
    List<LogEntry> logEntries = new List<LogEntry>();

    PopulateLists(peoples, logEntries);
    string filePathForPeople = @"D:\Projects & Planning\Learnings\advance-csharpe\StaticFiles\people.csv";
    string filePathForLog = @"D:\Projects & Planning\Learnings\advance-csharpe\StaticFiles\logEntries.csv";

    /* New way of doing things - generics */
    GenerateTextFileUsingGenerics.SaveToTextFile<People>(peoples, filePathForPeople);
    GenerateTextFileUsingGenerics.SaveToTextFile<LogEntry>(logEntries, filePathForLog);

    var newPeople = GenerateTextFileUsingGenerics.LoadFromTextFile<People>(filePathForPeople);

    foreach (var p in newPeople)
    {
        Console.WriteLine($"{ p.FirstName } { p.LastName } (IsAlive = { p.IsActive })");
    }

    var newLogs = GenerateTextFileUsingGenerics.LoadFromTextFile<LogEntry>(filePathForLog);

    foreach (var log in newLogs)
    {
        Console.WriteLine($"{ log.ErrorCode }: { log.Message } at { log.Date.ToShortTimeString() }");
    }

    //// save file
    //GenerateTextFile.SaveCSVData(peoples, filePathForPeople);

    //// retrive data to file
    //var data = GenerateTextFile.LoadCSVFile(filePathForPeople);

    //foreach(var entry in data)
    //{
    //    Console.WriteLine($"FirstName: {entry.FirstName}, LastName: {entry.LastName}, IsActive: {entry.IsActive}");
    //}

    Console.ReadLine();
}

void PopulateLists(List<People> peoples, List<LogEntry> logEntries)
{
    //peoples = new List<People>()
    //{
    //    new People(){FirstName="Rajib", LastName="Sarker", IsActive=true},
    //    new People(){FirstName="Raju", LastName="Sarker", IsActive=true},
    //    new People(){FirstName="Saju", LastName="Sarker", IsActive=true}
    //};

    peoples.Add(new People(){ FirstName = "Rajib", LastName = "Sarker", IsActive = true});
    peoples.Add(new People() { FirstName = "Raju", LastName = "Sarker", IsActive = true });
    peoples.Add(new People() { FirstName = "Saju", LastName = "Sarker", IsActive = true });


    //logEntries = new List<LogEntry>()
    //{
    //    new LogEntry(){ErrorCode=3232, Message="Error-1", Date=DateTime.Now.AddDays(-1) },
    //    new LogEntry(){ErrorCode=32332, Message="Error-2", Date=DateTime.Now.AddDays(-2) },
    //    new LogEntry(){ErrorCode=33232, Message="Error-3", Date=DateTime.Now.AddDays(-3) },
    //};

    logEntries.Add(new LogEntry() { ErrorCode = 3232, Message = "Error-1", Date = DateTime.Now.AddDays(-1) });
    logEntries.Add(new LogEntry() { ErrorCode = 32332, Message = "Error-2", Date = DateTime.Now.AddDays(-2) });
    logEntries.Add(new LogEntry() { ErrorCode = 33232, Message = "Error-3", Date = DateTime.Now.AddDays(-3) });
}