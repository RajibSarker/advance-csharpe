﻿using GenericsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsApp.WithoutGenerics
{
    public static class GenerateTextFile
    {
        public static void SaveCSVData(List<People> peoples, string filePath)
        {
            if (peoples == null || peoples.Count == 0 || string.IsNullOrEmpty(filePath)) throw new Exception("No data found to save or invalid path.");

            List<string> peopleToSave = new List<string>();

            peopleToSave.Add("FirstName,LastName,IsActive");

            foreach(var p in peoples)
            {
                var data = string.Format($"{p.FirstName},{p.LastName},{p.IsActive}");
                peopleToSave.Add(data);
            }
            File.WriteAllLines(filePath,peopleToSave);
        }

        public static List<People> LoadCSVFile(string filePath)
        {
            if (string.IsNullOrEmpty(filePath)) throw new Exception("No file to load data.");

            List<People> peoples = new List<People>();

            People people;

            var dataToRead = File.ReadAllLines(filePath).ToList();

            dataToRead.RemoveAt(0);
            foreach(var p in dataToRead)
            {
                var val = p.Split(",");
                people = new People();
                people.FirstName = val[0];
                people.LastName = val[1];
                people.IsActive = bool.Parse(val[2]);

                peoples.Add(people);
            }

            return peoples;
        }
    }
}
