namespace EventsApp
{
    public partial class Form1 : Form
    {
        Message message = new Message();
        public Form1()
        {
            InitializeComponent();
            OnLoad();
            message.MessageEvent += Message_MessageEvent;
        }

        private void Message_MessageEvent(object? sender, MessageArgs e)
        {
            if (e.Type == "Receiver")
            {
                listBox1.Items.Add(e.Message);
            }
            else
            {
                listBox1.Items.Add(e.Message.PadLeft(75));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var text = textBox1.Text;

            message.Send(new MessageArgs() { Type = "Sender", Message = textBox1.Text });
        }
        public void OnLoad()
        {
            Form2 form2 = new Form2(message);
            form2.Show();
        }
    }
}