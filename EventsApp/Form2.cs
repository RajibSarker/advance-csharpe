﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventsApp
{
    public partial class Form2 : Form
    {
        Message _message;
        public Form2(Message message)
        {
            InitializeComponent();
            _message = message;
            WireUpData();
        }

        private void WireUpData()
        {
            _message.MessageEvent += Message_MessageEvent;
        }

        private void Message_MessageEvent(object? sender, MessageArgs e)
        {
            if (e.Type == "Sender")
            {
                listBox1.Items.Add(e.Message);
            }
            else
            {
                listBox1.Items.Add(e.Message.PadLeft(75));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _message.Send(new MessageArgs() { Type = "Receiver", Message = textBox1.Text });
        }
    }
}
