﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsApp
{
    public class MessageArgs:EventArgs
    {
        public string Message { get; set; }
        public string Type { get; set; }
    }
}
