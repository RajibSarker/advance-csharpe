﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsApp
{
    public class Message
    {
        ////public event EventHandler<string> MessageEvent;

        ////public string MessageContent { get; set; }
        //public void Send(string message)
        //{
        //    //MessageEvent?.Invoke(this, message);
        //    OnMessageEvent(this, message);
        //}

        //public delegate void MessageEventHandler(object source, string message);
        //public event MessageEventHandler MessageEvent;

        //protected virtual void OnMessageEvent(object source, string message)
        //{
        //    MessageEvent?.Invoke(this, message);
        //}


        public event EventHandler<MessageArgs> MessageEvent;

        public void Send(MessageArgs messageArgs)
        {
            MessageEvent?.Invoke(this, messageArgs);
        }

    }
}
