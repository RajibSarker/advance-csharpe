﻿using DemoLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvanceCSharpe
{
    public class CallMethod2:AccessModifier
    {
        public void Method1()
        {
            base.ProtectedMethod();
        }
    }
}
