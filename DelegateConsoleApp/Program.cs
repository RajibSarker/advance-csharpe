﻿
using DelegateLibrary;



ShoppingCart cart = new ShoppingCart();

GenerateCartItems();
Console.WriteLine($"Total subtotal of the items: {cart.GenerateSubTotal(GetSubTotal, DynamicCalculationOfSubTotal, PrintAlert):C2}");
Console.WriteLine("..........................");
decimal subTotal = cart.GenerateSubTotal((subTotal)=> Console.WriteLine($"Total balance is: {subTotal}"),
    (balance) =>
    {
        if(balance > 1000)
        {
            return balance *= 0.80M;
        }
        else
        {
            return balance;
        }
    }, c=> Console.WriteLine(c));
Console.WriteLine($"Using anonymous method the total subTotal is: {subTotal:C2}");


void PrintAlert(string message)
{
    Console.WriteLine(message);
}

void GetSubTotal(decimal subTotal)
{
    Console.WriteLine($"The sub-totla is: {subTotal}");
}

void GenerateCartItems()
{
    cart.Products.Add(new Product { Name = "Product-1", Price = 44.32M });
    cart.Products.Add(new Product { Name = "Product-2", Price = 24.34M });
    cart.Products.Add(new Product { Name = "Product-3", Price = 8954.22M });
    cart.Products.Add(new Product { Name = "Product-4", Price = 84.52M });
}
decimal DynamicCalculationOfSubTotal(decimal subTotal)
{
    if (subTotal > 100)
    {
        return subTotal *= 0.80M;
    }
    else if (subTotal > 50)
    {
        return subTotal *= 0.85M;
    }
    else if (subTotal > 10)
    {
        return subTotal *= 0.90M;
    }
    else
    {
        return subTotal;
    }
}